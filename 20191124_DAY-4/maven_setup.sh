

root@jenkins:~# apt-get update 
Hit:1 http://in.archive.ubuntu.com/ubuntu bionic InRelease
Hit:2 http://in.archive.ubuntu.com/ubuntu bionic-updates InRelease
Hit:3 http://in.archive.ubuntu.com/ubuntu bionic-backports InRelease
Hit:4 http://in.archive.ubuntu.com/ubuntu bionic-security InRelease
Reading package lists... Done                      

root@jenkins:~# apt-get install maven -y 

root@jenkins:~# ls -lrt /usr/share/maven
total 16
lrwxrwxrwx 1 root root   10 Apr  9  2019 conf -> /etc/maven
drwxr-xr-x 2 root root 4096 Nov 24 17:03 man
drwxr-xr-x 2 root root 4096 Nov 24 17:03 lib
drwxr-xr-x 2 root root 4096 Nov 24 17:03 boot
drwxr-xr-x 2 root root 4096 Nov 24 17:03 bin
root@jenkins:~# 
root@jenkins:~# mvn --version
Apache Maven 3.6.0
Maven home: /usr/share/maven
Java version: 1.8.0_222, vendor: Private Build, runtime: /usr/lib/jvm/java-8-openjdk-amd64/jre
Default locale: en_US, platform encoding: UTF-8
OS name: "linux", version: "4.15.0-70-generic", arch: "amd64", family: "unix"

root@jenkins:~# cp -pv /etc/environment /etc/environment_20191124_BeforeMaven
'/etc/environment' -> '/etc/environment_20191124_BeforeMaven'

root@jenkins:~# vi /etc/environment

root@jenkins:~# cat /etc/environment
PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games"

# Java Home Path
JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64

# Maven Home Path
MAVEN_HOME=/usr/share/maven
M2=$MAVEN_HOME/bin

root@jenkins:~# source /etc/environment

root@jenkins:~# echo $MAVEN_HOME
/usr/share/maven

root@jenkins:~# echo $M2
/usr/share/maven/bin

