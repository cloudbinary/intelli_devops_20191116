
DevOps Engineer:

1. SDLC - Agile 
2. Operating Systems - 1. System Administration 
                       2. Network Administration 
3. Programming - Java, .net , NodeJs, Python etc .
4. Cloud - AWS, AZURE, GCP etc...

5. DevOps Tool Chain -
5.1. Download, Install and Configure 
    DevOps Tool Chain :
    0 Jira / Trello       - Continuous Planning 
    1 Git & GitHub/GitLab - Continuous Collabration and Version Control System 
    2 Java Project   
    3 Maven               - Build Tool 
    4 JUnit               - Continuous Unit Testing 
4.1 SonarQube           - Continuous Code Quality
    5 Jenkins             - Continuous Integration
6 Jfrog Artifactory   - Continuous Binary Code Repository
    7 Tomcat              - Application Server 
    8 Ansible, Puppet     - Configuration Management Tool 
    9 Docker              - Containerization 
    10 Kubernetes         - Container Orchestration 
    11 Nagios             - Continuous Monitoring Tool